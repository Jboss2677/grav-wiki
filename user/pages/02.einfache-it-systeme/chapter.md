---
title: 'Einfache IT-Systeme'
taxonomy:
    category:
        - docs
content:
    items: '@self.children'
    order:
        by: date
        dir: desc
    limit: 10
    pagination: true
---

### Lernfeld 4

# Einfache IT-Systeme

Delve deeper into more **complex** topics
