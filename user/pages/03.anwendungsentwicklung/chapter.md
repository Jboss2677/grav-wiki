---
title: Anwendungsentwicklung
taxonomy:
    category:
        - docs
content:
    items: '@self.children'
    order:
        by: date
        dir: desc
    limit: 10
    pagination: true
---

### Lernfeld 6

# Anwendungsentwicklung

Programmieren mit **Python**
