---
title: PAP
taxonomy:
    category:
        - docs
---

Voluptatem explicabo nihil iste repellendus unde eos animi saepe. Repellat quis autem et corrupti eaque nobis qui. Voluptas fuga autem aspernatur perspiciatis voluptatum pariatur eaque. Fuga similique vero alias sunt tempore ipsa. Blanditiis eligendi rerum nihil et expedita ut. Dolores porro enim et.

[flow]
st=>start: Start plugin
e=>end: End
op1=>operation: Development|success
sub1=>subroutine: Add features|success
cond=>condition: It is cool?|invalid
io=>inputoutput: Update for users|calm

st->op1->cond
cond(yes)->io->e
cond(no)->sub1(right)->op1
[/flow]