---
title: 'Der Betrieb und sein Umfeld'
content:
    items: '@self.children'
    order:
        by: date
        dir: desc
    limit: 10
    pagination: true
taxonomy:
    category:
        - docs
summary:
    enabled: true
    format: short
    size: int
---

### Lernfeld 1

# Der Betrieb und sein Umfeld

Summary
