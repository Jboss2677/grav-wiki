---
title: PoWi
taxonomy:
    category:
        - docs
content:
    items: '@self.children'
    order:
        by: date
        dir: desc
    limit: 10
    pagination: true
access:
    site.login: true
    admin.login: true
---

### PoWi

# Politik und Wirtschaft

Frau Schmidt