---
title: BBU
taxonomy:
    category:
        - docs
content:
    items: '@self.children'
    order:
        by: date
        dir: desc
    limit: 10
    pagination: true
---

### Berufsbildender Unterricht

# BBU