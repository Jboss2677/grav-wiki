# v1.1.0
## 07/06/2018

1. [](#bugfix)
    * Fixed caching problem (thanks to @pamtbaau on Discourse)

# v1.0.1
##  01/31/2017

1. [](#bugfix)
    * Fixed bug with shortcode not rendering with Admin plugin

# v1.0.0
##  10/30/2016

1. [](#new)
    * ChangeLog started...
