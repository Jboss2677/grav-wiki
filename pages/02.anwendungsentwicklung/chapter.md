---
title: Anwendungsentwicklung
editable-simplemde:
    self: false
editable: false
taxonomy:
    category:
        - docs
content:
    items: '@self.children'
    order:
        by: date
        dir: desc
    limit: 10
    pagination: true
---

### Lernfeld 6

# Anwendungs-</br>entwicklung

Programmieren mit **Python**
