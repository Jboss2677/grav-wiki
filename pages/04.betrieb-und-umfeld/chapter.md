---
title: 'Betrieb und Umfeld'
editable-simplemde:
    self: false
editable: false
taxonomy:
    category:
        - docs
content:
    items: '@self.children'
    order:
        by: date
        dir: desc
    limit: 10
    pagination: true
summary:
    enabled: true
    format: short
    size: int
---

### Lernfeld 1

# Betrieb und Umfeld

Summary
