<?php

/* chapter.html.twig */
class __TwigTemplate_968a5a62c61b91c6623142a181e537e1761af56e4120ee159437feacecc06144 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("docs.html.twig", "chapter.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "docs.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "\t<div id=\"chapter\">
    \t<div id=\"body-inner\">
\t\t\t";
        // line 6
        echo $this->getAttribute(($context["page"] ?? null), "content", array());
        echo "<br><br><br><br><br><br><br><br><br>
\t\t\t<h3>Inhalte</h3>
\t\t\t<hr>
\t\t\t";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute(($context["page"] ?? null), "collection", array()), "order", array(0 => "folder", 1 => "asc"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["p"]) {
            // line 10
            echo "\t\t\t\t<ul><a href=\"";
            echo $this->getAttribute($context["p"], "url", array());
            echo "\"><h4>";
            echo $this->getAttribute($context["p"], "title", array());
            echo "</h4></a>";
            echo \Grav\Common\Utils::truncate($this->getAttribute($context["p"], "summary", array()), 130, true);
            echo "</ul></br>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['p'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "\t\t</div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "chapter.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 12,  45 => 10,  41 => 9,  35 => 6,  31 => 4,  28 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'docs.html.twig' %}

{% block content %}
\t<div id=\"chapter\">
    \t<div id=\"body-inner\">
\t\t\t{{ page.content }}<br><br><br><br><br><br><br><br><br>
\t\t\t<h3>Inhalte</h3>
\t\t\t<hr>
\t\t\t{% for p in page.collection.order('folder','asc') %}
\t\t\t\t<ul><a href=\"{{p.url}}\"><h4>{{ p.title}}</h4></a>{{p.summary|truncate(130, true)}}</ul></br>
\t\t\t{% endfor %}
\t\t</div>
    </div>
{% endblock %}
", "chapter.html.twig", "D:\\websites\\grav-doc\\user\\themes\\doc-theme\\templates\\chapter.html.twig");
    }
}
