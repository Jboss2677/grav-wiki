<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'themes://doc-theme/doc-theme.yaml',
    'modified' => 1538649500,
    'data' => [
        'streams' => [
            'schemes' => [
                'theme' => [
                    'type' => 'ReadOnlyStream',
                    'prefixes' => [
                        '' => [
                            0 => 'user/themes/doc-theme',
                            1 => 'user/themes/learn2'
                        ]
                    ]
                ]
            ]
        ]
    ]
];
